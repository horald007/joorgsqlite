<?php
$listarray = array ( array ( 'label' => 'Bezeichnung',
                             'name' => 'bez', 
                             'width' => 200, 
                             'type' => 'text',
                             'dbfield' => 'fldbez' ),
                     array ( 'label' => 'Hintergrundfarbe',
                             'name' => 'hntfarbe', 
                             'width' => 100, 
                             'type' => 'text',
                             'dbfield' => 'fldfarbe' ),
                     array ( 'label' => 'Textfarbe',
                             'name' => 'txtfarbe', 
                             'width' => 100, 
                             'type' => 'text',
                             'dbfield' => 'fldtxtfarbe' ));

/*
$filterarray = array ( array ( 'label' => 'Gruppe',
                             'name' => 'gruppe', 
                             'width' => 10, 
                             'type' => 'selectid',
                             'sign' => '=',
                             'dbtable' => 'tbltermine_grp',
                             'seldbfield' => 'fldbez',
                             'seldbindex' => 'fldindex',
                             'dbfield' => 'fldid_terminegrp' ),
                       array ( 'label' => 'Serie',
                             'name' => 'serie', 
                             'width' => 10, 
                             'type' => 'selectid',
                             'sign' => '=',
                             'dbtable' => 'tbltermine_serie',
                             'seldbfield' => 'fldbez',
                             'seldbindex' => 'fldindex',
                             'dbfield' => 'fldid_terminserie' ),
                     array ( 'label' => '<br>Zeitraum von:',
                             'name' => 'zeitraumvon',
                             'value' => '', 
                             'width' => 500, 
                             'type' => 'date',
                             'sign' => '>=',                              
                             'dbfield' => 'fldvondatum' ),
                     array ( 'label' => 'Zeitraum bis:',
                             'name' => 'zeitraumbis',
                             'value' => '', 
                             'width' => 500, 
                             'type' => 'date',
                             'sign' => '<=',                              
                             'dbfield' => 'fldvondatum' ));
*/

$pararray = array ( 'headline' => 'Termingruppen',
                    'dbtable' => 'tbltermine_grp',
                    'orderby' => '',
                    'strwhere' => '',
                    'fldindex' => 'fldindex');
?>