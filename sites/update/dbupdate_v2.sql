INSERT INTO tblmenu_liste (fldmenu,fldbez,fldview,fldid_parent,fldusermenu,fldglyphicon,fldsort) VALUES ('SUBMENU','Privat','J','0','N','glyphicon-file','005')
INSERT INTO tblmenu_liste (fldmenu,fldbez,fldview,fldid_parent,fldusermenu,fldglyphicon,fldsort) VALUES ('stdplan','Stundenplan','J','46','N','glyphicon-file','001')
DROP TABLE IF EXISTS `tblstdplan`;
CREATE TABLE `tblstdplan` (`fldindex` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,`fldid_std` bigint(20) NOT NULL,`fldid_wotag` bigint(20) NOT NULL,`fldbez` varchar(250) NOT NULL,`fldid_user` bigint(20) NOT NULL,`fldnr` varchar(3) NOT NULL,`flddbsyncstatus` varchar(10) NOT NULL DEFAULT 'SYNC');
INSERT INTO tblmenu_liste (fldmenu,fldbez,fldview,fldid_parent,fldusermenu,fldglyphicon,fldsort) VALUES ('stdzeit','Zeiten','J','46','N','','002')
DROP TABLE IF EXISTS `tblstdzeit`;
CREATE TABLE `tblstdzeit` (`fldindex` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,`fldbez` varchar(20) NOT NULL,"flddbsyncstatus" text NOT NULL DEFAULT 'SYNC');
INSERT INTO tblmenu_liste (fldmenu,fldbez,fldview,fldid_parent,fldusermenu,fldglyphicon,fldsort) VALUES ('stdwotag','Wochentag','J','46','N','','003')
DROP TABLE IF EXISTS `tblstdwotag`;
CREATE TABLE `tblstdwotag` (`fldindex` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,`fldspalte` varchar(5) NOT NULL,`fldbez` varchar(250) NOT NULL,`fldfarbe` varchar(8) NOT NULL);
INSERT INTO tblmenu_liste (fldmenu,fldbez,fldview,fldid_parent,fldusermenu,fldglyphicon,fldsort) VALUES ('user','Benutzer','J','39','N','glyphicon-user','003')
DROP TABLE IF EXISTS `tblbenutzer`;
CREATE TABLE `tblbenutzer` (`fldindex` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,`fldbez` varchar(200) DEFAULT NULL,`fldtimestamp` varchar(50) NOT NULL DEFAULT 'CURRENT_TIMESTAMP',`fldbackgroundfilename` char(250) DEFAULT NULL,`fldusername` char(50) DEFAULT NULL,`fldbelohn` char(1) NOT NULL DEFAULT 'N',`fldbelohnpkte` bigint(20) DEFAULT NULL,`fldbelohngrenze` bigint(20) DEFAULT NULL,`fldfamkal` char(1) NOT NULL DEFAULT 'N',`fldgebkal` char(1) NOT NULL DEFAULT 'N',`fldstartuser` char(1) NOT NULL DEFAULT 'N',`fldreihenfolge` bigint(20) NOT NULL DEFAULT '999', `flddbsyncstatus` char(10) NOT NULL DEFAULT 'SYNC');
INSERT INTO tblfunc (fldBez,fldName,fldphp,fldMenuID,fldTarget,fldParam,fldTyp,fldlanguage) VALUES ('Stundenplan','','druckstundenplan.php','stdplan','_blank','','','')